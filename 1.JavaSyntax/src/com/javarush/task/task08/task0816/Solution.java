package com.javarush.task.task08.task0816;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* 
Добрая Зинаида и летние каникулы
*/

public class Solution {
    public static HashMap<String, Date> createMap() throws ParseException {
        DateFormat df = new SimpleDateFormat("MMMMM d yyyy", Locale.ENGLISH);
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Stallone", df.parse("JUNE 1 1980"));
        map.put("Stallone1", df.parse("JULY 1 1980"));
        map.put("Stallone2", df.parse("AUGUST 1 1980"));
        map.put("Stallone3", df.parse("SEPTEMBER 1 1980"));
        map.put("Stallone4", df.parse("SEPTEMBER 1 1980"));
        map.put("Stallone5", df.parse("SEPTEMBER 1 1980"));
        map.put("Stallone6", df.parse("JUNE 1 1980"));
        map.put("Stallone7", df.parse("SEPTEMBER 1 1980"));
        map.put("Stallone8", df.parse("JUNE 1 1980"));
        map.put("Stallone9", df.parse("SEPTEMBER 1 1980"));
        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map) {
        for(Map.Entry<String, Date> pair : map.entrySet()) {
            Date value = pair.getValue();
            if(pair.toString().contains("JUNE") || pair.toString().contains("JULY") || pair.toString().contains("AUGUST")) {

            }
        }

    }

    public static void main(String[] args) {

    }
}
