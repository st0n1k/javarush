package com.javarush.task.task07.task0721;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
Минимаксы в массивах
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int maximum = Integer.MIN_VALUE;
        int minimum = Integer.MAX_VALUE;

        int[] arr = new int[20];
        for(int i = 0; i < arr.length; i++) {
            String s = reader.readLine();
            arr[i] = Integer.parseInt(s);
        }
        for(int i = 0; i < arr.length; i++) {
            if(arr[i] > maximum) maximum = arr[i];
            if(arr[i] < minimum) minimum = arr[i];
        }
        //напишите тут ваш код

        System.out.print(maximum + " " + minimum);
    }
}
