package com.javarush.task.task16.task1632;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static List<Thread> threads = new ArrayList<>(5);
    static {
        threads.add(new ThreadInfinitely());
        threads.add(new ThreadInterrupted());
        threads.add(new ThreadUra());
        threads.add(new ThreadMessage());
        threads.add(new ThreadNumberInputs());
    }

    public static void main(String[] args) {
        for(Thread th : threads) {
            th.start();
        }
    }

    public static class ThreadInfinitely extends Thread {
        @Override
        public void run() {
            while(true) {

            }
        }
    }

    public static class ThreadInterrupted extends Thread {
        @Override
        public void run() {
            try {
                Thread.sleep(2000000000);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException");
            }
        }
    }

    public static class ThreadUra extends Thread {
        @Override
        public void run() {
            try {
                while(true) {
                    System.out.println("Ура");
                    Thread.sleep(500);
                }
            } catch (InterruptedException e) {
            }
        }
    }

    public static class ThreadMessage extends Thread implements Message {
        private boolean die;

        @Override
        public void run() {
            while(die == false) {

            }
        }

        @Override
        public void showWarning() {
            die = true;
        }
    }

    public static class ThreadNumberInputs extends Thread {
        @Override
        public void run() {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            int sum = 0;
            String s = null;
            while(true) {
                try {
                    s = br.readLine();
                } catch(IOException e) {

                }
                if(s.equals("N") == true) {
                    break;
                }
                sum += Integer.parseInt(s);
            }
            System.out.println(sum);
        }

    }
}