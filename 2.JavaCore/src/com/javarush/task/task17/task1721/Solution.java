package com.javarush.task.task17.task1721;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Транзакционность
*/

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String file1 = null, file2 = null;
        try {
            file1 = br.readLine();
            file2 = br.readLine();
        } catch(IOException e) {

        }

        try {
            BufferedReader fileRD1 = new BufferedReader(new InputStreamReader(new FileInputStream(file1)));
            while(fileRD1.ready()) {
                allLines.add(fileRD1.readLine());
            }
        } catch(FileNotFoundException e) {

        } catch (IOException e) {

        }

        try {
            BufferedReader fileRD2 = new BufferedReader(new InputStreamReader(new FileInputStream(file2)));
            while(fileRD2.ready()) {
                forRemoveLines.add(fileRD2.readLine());
            }
        } catch(FileNotFoundException e) {

        } catch(IOException e) {

        }

        try {
            new Solution().joinData();
        } catch(CorruptedDataException e) {

        }


    }

    public void joinData() throws CorruptedDataException {
        if(allLines.containsAll(forRemoveLines)) {
            allLines.removeAll(forRemoveLines);
        } else {
            allLines.clear();
            throw new CorruptedDataException();
        }
    }
}
