package com.javarush.task.task17.task1710;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {
        DateFormat date1 = new SimpleDateFormat("dd/mm/yyyy", Locale.ENGLISH);
        DateFormat date2 = new SimpleDateFormat("dd-mmm-yyyy", Locale.ENGLISH);

        // create
        if(args[0].startsWith("-c")) {
            Date date = null;
            try {
                date = date1.parse(args[3]);
            } catch(ParseException e) {

            }

            if(args[1].startsWith("м")) {
                Person person = Person.createMale(args[1], date);
                allPeople.add(person);
                System.out.println(allPeople.indexOf(person));
            } else {
                Person person = Person.createFemale(args[1], date);
                allPeople.add(person);
                System.out.println(allPeople.indexOf(person));
            }
        }
        // update
        if(args[0].startsWith("-u")) {
            int index = Integer.parseInt(args[1]);
            Person person = allPeople.get(index);

            //name
            person.setName(args[2]);

            //date
            Date date = null;
            try {
                date = date1.parse(args[4]);
            } catch (ParseException e) {

            }
            person.setBirthDate(date);

            //sex
            if (args[3].startsWith("м"))
                person.setSex(Sex.MALE);
            else
                person.setSex(Sex.FEMALE);
        }

        //delete
        if(args[0].startsWith("-d")) {
            int index = Integer.parseInt(args[1]);
            Person person = allPeople.get(index);

            person.setName(null);
            person.setBirthDate(null);
            person.setSex(null);
        }

        //output
        if(args[0].startsWith("-i")) {
            int index = Integer.parseInt(args[1]);
            Person person = allPeople.get(index);
            StringBuffer s = new StringBuffer();
            s.append(person.getName());
            s.append(" ");
            s.append(person.getSex() == Sex.MALE ? "м" : "ж");
            s.append(" ");
            s.append(date2.format(person.getBirthDate()));
            System.out.println(s);
        }
    }
}
