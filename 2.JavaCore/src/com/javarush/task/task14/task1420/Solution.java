package com.javarush.task.task14.task1420;

/* 
НОД
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        String s1 = br.readLine();
        int num1 = Integer.parseInt(s);
        int num2 = Integer.parseInt(s1);
        if(num1 <= 0 || num2 <= 0) throw new Exception();
        int max = Math.max(num1, num2);
        for(int i = max; i > 0; i--) {
            if(num1 % i == 0 && num2 % i == 0) {
                System.out.println(i);
                break;
            }
        }
    }
}
