package com.javarush.task.task15.task1529;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;/*
Осваивание статического блока
*/

public class Solution {
    public static void main(String[] args) {

    }
    
    static {
        reset();
    }

    public static CanFly result;

    public static void reset() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String s = br.readLine();
            if(s.equals("helicopter")) {
                result = new Helicopter();
            } else
                if(s.equals("plane")) {
                    String s1 = br.readLine();
                    int passangers = Integer.parseInt(s1);
                    result = new Plane(passangers);
                }
                br.close();
        } catch(IOException e) {

        }
    }
}
