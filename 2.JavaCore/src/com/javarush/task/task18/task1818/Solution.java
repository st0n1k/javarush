package com.javarush.task.task18.task1818;

/* 
Два в одном
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        String fileName1 = br.readLine();
        String fileName2 = br.readLine();
        br.close();
        FileOutputStream file1 = new FileOutputStream(fileName);
        FileInputStream file2 = new FileInputStream(fileName1);
        FileInputStream file3 = new FileInputStream(fileName2);
        while (file2.available() > 0) {
            byte[] buffer = new byte[file2.available()];
            int count = file2.read(buffer);
            file1.write(buffer, 0, count);
        }
        file2.close();
        while (file3.available() > 0) {
            byte[] buff = new byte[file3.available()];
            int count = file3.read(buff);
            file1.write(buff);
        }
        file1.close();
        file3.close();
    }
}
