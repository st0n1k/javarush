package com.javarush.task.task18.task1810;

/* 
DownloadException
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws DownloadException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            try {
                String fileName = br.readLine();
                FileInputStream file = new FileInputStream(fileName);
                int bytes = 0;
                while(file.available() > 0) {
                    int value = file.read();
                    bytes++;
                }
                if(bytes < 1000) {
                    file.close();
                    br.close();
                    throw new DownloadException();
                }
            } catch (IOException e) {

            }
        }
    }

    public static class DownloadException extends Exception {

    }
}
