package com.javarush.task.task18.task1816;

/* 
Английские буквы
*/

import java.io.FileReader;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        String alphabetLower = "abcdefghijklmnopqrstuvwxyz";
        String alphabetUpper = alphabetLower.toUpperCase();
        FileReader reader = new FileReader(args[0]);
        int counter = 0;
        while(reader.ready()) {
            char bytes = (char) reader.read();
            if(alphabetLower.indexOf(bytes) > -1 || alphabetUpper.indexOf(bytes) > -1) {
                counter++;
            }
        }
        reader.close();
        System.out.println(counter);
    }
}
