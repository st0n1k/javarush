package com.javarush.task.task18.task1804;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* 
Самые редкие байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        FileInputStream file = new FileInputStream(fileName);
        Map<Integer, Integer> map = new HashMap<>();
        while(file.available() > 0) {
            int value = file.read();
            if(map.containsKey(value)) {
                map.put(value, map.get(value) + 1);
            } else {
                map.put(value,1);
            }
        }

        int min = 100;
        for(Map.Entry entry : map.entrySet()) {
            if ((int) entry.getValue() < min) min = (int) entry.getValue();
        }
        for (Map.Entry entry: map.entrySet()) {
            if(entry.getValue().equals(min)) System.out.print(entry.getKey() + " ");
        }
        br.close();
        file.close();
    }
}
