package com.javarush.task.task18.task1808;

/* 
Разделение файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            String fileName1 = br.readLine();
            String fileName2 = br.readLine();
            String fileName3 = br.readLine();
            FileInputStream file1 = new FileInputStream(fileName1);
            FileOutputStream file2 = new FileOutputStream(fileName2);
            FileOutputStream file3 = new FileOutputStream(fileName3);
            int lengthSecondFile;
            byte[] buffer = new byte[file1.available()];
            while (file1.available() > 0) {

                if (file1.available() % 2 == 0) {
                    lengthSecondFile = file1.available() / 2;
                } else
                    lengthSecondFile = file1.available() / 2 + 1;

                byte[] bufferForSecondFile = new byte[lengthSecondFile];
                byte[] bufferForThreadFile = new byte[file1.available() / 2];

                file2.write(bufferForSecondFile, 0, file1.read(bufferForSecondFile));

                file3.write(bufferForThreadFile, 0, file1.read(bufferForThreadFile));
            }
            br.close();
            file1.close();
            file2.close();
            file3.close();
        } catch(IOException e) {

        }
    }
}
