package com.javarush.task.task18.task1817;

/* 
Пробелы
*/

import java.io.FileReader;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileReader reader = new FileReader(args[0]);
        int countSymb = 0, countSpace = 0;
        while(reader.ready()) {
            char symbols = (char) reader.read();
            countSymb++;
            if(symbols == ' ') countSpace++;

        }
        System.out.printf("%.2f", ((float)countSpace/countSymb) * 100);
        reader.close();
    }
}
