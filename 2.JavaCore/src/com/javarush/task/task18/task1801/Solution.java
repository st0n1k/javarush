package com.javarush.task.task18.task1801;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;/*
Максимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        FileInputStream file = new FileInputStream(fileName);
        int maxByte = 0;
        while(file.available() > 0) {
            int value = file.read();
            if(value > maxByte) maxByte = value;
        }
        System.out.println(maxByte);
        file.close();
    }
}
