package com.javarush.task.task18.task1807;

/* 
Подсчет запятых
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int counter = 0;
        try {
            String fileName = br.readLine();
            FileInputStream file = new FileInputStream(fileName);
            while(file.available() > 0) {
                char value = (char) file.read();
                if(value == ',') {
                    counter++;
                }
            }
            file.close();
        } catch (IOException e) {
        }
        System.out.println(counter);


    }
}
