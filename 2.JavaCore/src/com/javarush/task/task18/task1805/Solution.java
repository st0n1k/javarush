package com.javarush.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.TreeSet;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        FileInputStream file = new FileInputStream(fileName);
        TreeSet<Integer> tree = new TreeSet<>();
        while(file.available() > 0) {
            tree.add(file.read());
        }
        file.close();
        for(int value : tree) {
            System.out.print(value + " ");
        }
    }
}
