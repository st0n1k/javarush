package com.javarush.task.task18.task1809;

/* 
Реверс файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            String fileName1 = br.readLine();
            String fileName2 = br.readLine();
            FileInputStream file1 = new FileInputStream(fileName1);
            FileOutputStream file2 = new FileOutputStream(fileName2);
            byte[] buffer = new byte[file1.available()];
            byte[] reverse = new byte[buffer.length];
            while(file1.available() > 0) {
                file1.read(buffer);
                for(int i = buffer.length - 1; i >= 0; i--) {
                    reverse[buffer.length - i - 1] = buffer[i];
                }
                file2.write(reverse);
            }
            file1.close();
            file2.close();
            br.close();
        } catch (IOException e) {

        }

    }
}
