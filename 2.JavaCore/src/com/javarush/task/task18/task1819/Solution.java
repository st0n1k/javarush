package com.javarush.task.task18.task1819;

/* 
Объединение файлов
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = br.readLine();
        String fileName2 = br.readLine();
        br.close();
        FileInputStream file1input = new FileInputStream(fileName1);
        FileInputStream file2 = new FileInputStream(fileName2);
            byte[] buffer = new byte[file1input.available()];
            file1input.read(buffer);
            file1input.close();
            byte[] buff = new byte[file2.available()];
            file2.read(buff);
            file2.close();
        FileOutputStream file1 = new FileOutputStream(fileName1);
        file1.write(buff);
        file1.write(buffer);
        file1.close();
    }
}
