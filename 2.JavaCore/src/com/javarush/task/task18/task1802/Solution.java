package com.javarush.task.task18.task1802;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String fileName = br.readLine();
        FileInputStream file = new FileInputStream(fileName);
        int min = 100;
        while(file.available() > 0) {
            int value = file.read();
            if(value < min) min = value;
        }
        System.out.println(min);
        file.close();
    }
}
