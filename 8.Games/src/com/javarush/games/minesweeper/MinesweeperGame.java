package com.javarush.games.minesweeper;

import com.javarush.engine.cell.Color;
import com.javarush.engine.cell.Game;

public class MinesweeperGame extends Game {
    private static final int SIDE = 9;
    private int countMinesOnField;
    boolean mine = false;
    @Override
    public void initialize() {
        setScreenSize(SIDE, SIDE);
        createGame();
    }

    private GameObject[][] gameField = new GameObject[SIDE][SIDE];

    private void createGame() {
        for(int x = 0; x < SIDE; x++) {
            for(int y = 0; y < SIDE; y++) {
                setCellColor(x,y, Color.ORANGE);
                if(getRandomNumber(10) == 9) {
                    setCellColor(x, y, Color.ORANGE);
                    gameField[y][x] = new GameObject(x,y,mine);
                    this.mine = true;
                    countMinesOnField++;
                }
            }
            System.out.println(countMinesOnField);
        }
    }
}
